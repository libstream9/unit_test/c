#ifndef STREAM9_C_TEST_SRC_NAMESPACE_HPP
#define STREAM9_C_TEST_SRC_NAMESPACE_HPP

namespace stream9::c {}
namespace stream9::strings {}

namespace testing {

namespace c { using namespace stream9::c; }
namespace str { using namespace stream9::strings; }

} // namespace testing


#endif // STREAM9_C_TEST_SRC_NAMESPACE_HPP
