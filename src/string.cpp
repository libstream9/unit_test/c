#include <stream9/c/string.hpp>

#include "namespace.hpp"

#include <utility>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(cstring_)

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        c::string s1;
        c::const_string s2;
    }

    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        c::string s1 { ::strdup("foo") };
        BOOST_TEST(s1 == "foo");

        c::const_string s2 { ::strdup("bar") };
        BOOST_TEST(s2 == "bar");
    }

    BOOST_AUTO_TEST_CASE(move_constructor_)
    {
        c::const_string s1 { ::strdup("foo") };

        auto s2 { std::move(s1) };

        BOOST_TEST(s1.empty());
        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(move_assignment_)
    {
        c::string s1 { ::strdup("foo") };
        c::string s2;

        s2 = std::move(s1);

        BOOST_TEST(s1.empty());
        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(swap_)
    {
        c::const_string s1 { ::strdup("foo") };
        c::const_string s2 { ::strdup("bar") };

        using std::swap;
        swap(s1, s2);

        BOOST_TEST(s1 == "bar");
        BOOST_TEST(s2 == "foo");
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        c::string s1 { ::strdup("foo") };

        auto it = s1.begin();
        auto end = s1.end();

        static_assert(std::same_as<decltype(it), decltype(s1)::iterator>);
        static_assert(std::same_as<decltype(end), decltype(s1)::sentinel>);

        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'f');
        *it = 'b';

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');
        *it = 'a';

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');
        *it = 'r';

        ++it;
        BOOST_REQUIRE(it == end);

        BOOST_TEST(s1 == "bar");
    }

    BOOST_AUTO_TEST_CASE(const_iterator_1_)
    {
        c::string const s1 { ::strdup("foo") };

        auto it = s1.begin();
        auto end = s1.end();

        static_assert(std::same_as<decltype(it), decltype(s1)::const_iterator>);
        static_assert(std::same_as<decltype(end), decltype(s1)::sentinel>);

        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'f');

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');

        ++it;
        BOOST_REQUIRE(it == end);
    }

    BOOST_AUTO_TEST_CASE(const_iterator_2_)
    {
        c::const_string s1 { ::strdup("foo") };

        auto it = s1.begin();
        auto end = s1.end();

        static_assert(std::same_as<decltype(it), decltype(s1)::const_iterator>);
        static_assert(std::same_as<decltype(end), decltype(s1)::sentinel>);

        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'f');

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');

        ++it;
        BOOST_REQUIRE(it != end);
        BOOST_TEST(*it == 'o');

        ++it;
        BOOST_REQUIRE(it == end);
    }

    BOOST_AUTO_TEST_CASE(iterator_empty_1_)
    {
        c::string s1;

        auto it = s1.begin();
        auto end = s1.end();

        BOOST_REQUIRE(it == end);
    }

    BOOST_AUTO_TEST_CASE(iterator_empty_2_)
    {
        c::string s1 { ::strdup("") };

        auto it = s1.begin();
        auto end = s1.end();

        BOOST_REQUIRE(it == end);
    }

    BOOST_AUTO_TEST_CASE(subscript_)
    {
        c::string s1 { ::strdup("foo") };

        BOOST_TEST(s1[0] == 'f');
        BOOST_TEST(s1[1] == 'o');
        BOOST_TEST(s1[2] == 'o');
        BOOST_TEST(s1[3] == '\0');
    }

    BOOST_AUTO_TEST_CASE(front_)
    {
        c::string s1 { ::strdup("foo") };

        s1.front() = 'b';
        BOOST_TEST(s1 == "boo");
    }

    BOOST_AUTO_TEST_CASE(const_front_)
    {
        c::const_string const s1 { ::strdup("foo") };

        auto&& c = s1.front();

        static_assert(std::same_as<decltype(c), decltype(s1)::const_reference>);
        BOOST_TEST(c == 'f');
    }

    BOOST_AUTO_TEST_CASE(data_)
    {
        c::const_string s1 { ::strdup("foo") };

        BOOST_TEST(::strcmp(s1.data(), "foo") == 0);
    }

    BOOST_AUTO_TEST_CASE(data_const_)
    {
        c::string const s1;

        BOOST_TEST(s1.data() == nullptr);
    }

    BOOST_AUTO_TEST_CASE(c_str_)
    {
        c::const_string s1 { ::strdup("foo") };

        BOOST_TEST(::strcmp(s1.c_str(), "foo") == 0);
    }

    BOOST_AUTO_TEST_CASE(c_str_empty_)
    {
        c::string s1;

        BOOST_TEST(s1.data() == nullptr);
        BOOST_TEST(s1.c_str() != nullptr);
        BOOST_TEST(::strcmp(s1.c_str(), "") == 0);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        c::string s1 { ::strdup("foo") };

        BOOST_TEST(!s1.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_on_empty_1_)
    {
        c::string s1;

        BOOST_TEST(s1.empty());
    }

    BOOST_AUTO_TEST_CASE(empty_on_empty_2_)
    {
        c::string s1 { ::strdup("") };

        BOOST_TEST(s1.empty());
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        c::const_string s1 { ::strdup("foo") };

        BOOST_TEST(s1.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(size_empty_1_)
    {
        c::string s1;

        BOOST_TEST(s1.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(size_empty_2_)
    {
        c::string s1 { ::strdup("") };

        BOOST_TEST(s1.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(reset_)
    {
        c::string s1 { ::strdup("foo") };

        s1.reset(::strdup("bar"));
        BOOST_TEST(s1 == "bar");
    }

    BOOST_AUTO_TEST_CASE(reset_empty_)
    {
        c::string s1 { ::strdup("foo") };

        s1.reset(nullptr);
        BOOST_TEST(s1.empty());
    }

    BOOST_AUTO_TEST_CASE(release_)
    {
        c::const_string s1 { ::strdup("foo") };

        auto&& p = s1.release();

        BOOST_TEST(::strcmp(p, "foo") == 0);
        BOOST_TEST(s1.empty());

        free((void*)p);
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view_)
    {
        c::const_string s1 { ::strdup("foo") };

        std::string_view sv = s1;

        BOOST_TEST(sv == "foo");
    }

    BOOST_AUTO_TEST_CASE(convert_to_string_view_empty_)
    {
        c::const_string s1;

        std::string_view sv = s1;

        BOOST_TEST(sv.empty());
    }

    BOOST_AUTO_TEST_CASE(convert_to_cstring_view_)
    {
        c::const_string s1 { ::strdup("foo") };

        str::cstring_view sv = s1;

        BOOST_TEST(sv == "foo");
    }

    BOOST_AUTO_TEST_CASE(convert_to_std_string_1_)
    {
        c::const_string s1 { ::strdup("foo") };

        std::string s { s1 };

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(convert_to_std_string_2_)
    {
        c::const_string s1 { ::strdup("foo") };

        std::string s = std::string(s1); // because converter is marked explicit

        BOOST_TEST(s == "foo");
    }

    BOOST_AUTO_TEST_CASE(convert_to_std_string_empty_)
    {
        c::const_string s1;

        std::string s { s1 };

        BOOST_TEST(s.empty());
    }

BOOST_AUTO_TEST_SUITE_END() // cstring_

} // namespace testing
