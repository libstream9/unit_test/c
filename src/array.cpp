#include <stream9/c/array.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(array_)

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        c::array<int> a;

        BOOST_TEST(a.empty());
    }

    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        auto* p = static_cast<char**>(::calloc(4, sizeof(char*)));
        p[0] = ::strdup("foo");
        p[1] = ::strdup("bar");
        p[2] = ::strdup("xyzzy");
        p[3] = nullptr;

        c::array<char*> a { p };

        BOOST_TEST(a.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        auto* p = static_cast<char**>(::calloc(4, sizeof(char*)));
        p[0] = ::strdup("foo");
        p[1] = ::strdup("bar");
        p[2] = ::strdup("xyzzy");
        p[3] = nullptr;

        c::array<char*> a1 { p };
        BOOST_TEST(a1.size() == 3);

        c::array<char*> a2 { std::move(a1) };
        BOOST_TEST(a1.empty());
        BOOST_TEST(a2.size() == 3);
    }

    BOOST_AUTO_TEST_CASE(reset_)
    {
        auto* p1 = static_cast<int*>(::calloc(4, sizeof(int)));
        p1[0] = 1; p1[1] = 2; p1[2] = 3; p1[3] = 0;

        c::array<int> a { p1 };

        BOOST_REQUIRE(a.size() == 3);
        BOOST_TEST(a[0] == 1);
        BOOST_TEST(a[1] == 2);
        BOOST_TEST(a[2] == 3);

        auto* p2 = static_cast<int*>(::calloc(4, sizeof(int)));
        p2[0] = 3; p2[1] = 2; p2[2] = 1; p2[3] = 0;

        a.reset(p2);

        BOOST_REQUIRE(a.size() == 3);
        BOOST_TEST(a[0] == 3);
        BOOST_TEST(a[1] == 2);
        BOOST_TEST(a[2] == 1);
    }

    BOOST_AUTO_TEST_CASE(release_)
    {
        auto* p1 = static_cast<int*>(::calloc(4, sizeof(int)));
        p1[0] = 1; p1[1] = 2; p1[2] = 3; p1[3] = 0;

        c::array<int> a { p1 };

        BOOST_REQUIRE(a.size() == 3);
        BOOST_TEST(a[0] == 1);
        BOOST_TEST(a[1] == 2);
        BOOST_TEST(a[2] == 3);

        auto p2 = a.release();
        BOOST_TEST(a.empty());

        ::free(p2);
    }

    BOOST_AUTO_TEST_CASE(equal_)
    {
        auto* p1 = static_cast<int*>(::calloc(4, sizeof(int)));
        p1[0] = 1; p1[1] = 2; p1[2] = 3; p1[3] = 0;

        c::array<int> a1 { p1 };

        BOOST_CHECK(a1 == a1);

        auto* p2 = static_cast<int*>(::calloc(4, sizeof(int)));
        p2[0] = 3; p2[1] = 2; p2[2] = 1; p2[3] = 0;

        c::array<int> a2 { p2 };

        BOOST_CHECK(a1 != a2);
    }

    BOOST_AUTO_TEST_CASE(compare_)
    {
        auto* p1 = static_cast<int*>(::calloc(4, sizeof(int)));
        p1[0] = 1; p1[1] = 2; p1[2] = 3; p1[3] = 0;

        c::array<int> a1 { p1 };

        auto* p2 = static_cast<int*>(::calloc(3, sizeof(int)));
        p2[0] = 1; p2[1] = 2; p2[2] = 0;

        c::array<int> a2 { p2 };

        BOOST_CHECK(a1 > a2);
        BOOST_CHECK(a2 < a1);
        BOOST_CHECK(a1 >= a2);
        BOOST_CHECK(a2 <= a1);
    }

BOOST_AUTO_TEST_SUITE_END() // array_

} // namespace testing
